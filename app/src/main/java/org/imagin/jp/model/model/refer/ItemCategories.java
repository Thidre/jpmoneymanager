package org.imagin.jp.model.model.refer;

/**
 * Created by Thibault on 02/03/2016.
 */
public enum ItemCategories {
    /**
     * Not set yet.
     */
    None,
    /**
     * Main meal.
     */
    Main,
    /**
     * Things that comes with Main.
     */
    Garniture,
    /*
    * Any drink.
     */
    Drink,
    /*
      * Miam !
     */
    Dessert,
    /**
     * Not put in a category
     */
    Unsorted,
    /**
     * Item that contain other items.
     */
    Menu;

    public static ItemCategories fromInteger(int x) {
        switch (x) {
            default:
                return None;
            case 1:
                return Main;
            case 2:
                return Garniture;
            case 3:
                return Dessert;
            case 4:
                return Drink;
            case 5:
                return Unsorted;
            case 6:
                return Menu;
        }
    }

    public static int asInt(ItemCategories category) {
        switch (category) {
            default:
                return 0;
            case Main:
                return 1;
            case Garniture:
                return 2;
            case Drink:
                return 4;
            case Dessert:
                return 3;
            case Unsorted:
                return 5;
            case Menu:
                return 6;
        }
    }

    public static String toString(ItemCategories category) {
        switch (category) {
            default:
                return "Inconnu";
            case Main:
                return "Plat principal";
            case Garniture:
                return "Accompagnement";
            case Drink:
                return "Boisson";
            case Dessert:
                return "Dessert";
            case Unsorted:
                return "Non classé";
            case Menu:
                return "Menu";
        }
    }
}