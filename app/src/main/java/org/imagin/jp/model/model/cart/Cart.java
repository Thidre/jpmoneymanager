package org.imagin.jp.model.model.cart;

import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp.model.model.refer.ItemCategories;
import org.imagin.jp.model.model.refer.Menu;
import org.imagin.jp.model.model.refer.Sale;
import org.imagin.jp.model.model.refer.SellingPoint;
import org.imagin.jp.view.util.SaleItemButton;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Shopping cart. Contains the items that currently forms user's order.
 */
public class Cart {

    /**
     * Components of current user's order.
     */
    private List<ItemOrder> orders;
    private MenuOrder menuBeingBuild;

    /**
     *
     * @return Get Components of current user's order.
     */
    public final List<ItemOrder> getOrders() {
        return orders;
    }

    public Cart() {
        orders = new LinkedList<ItemOrder>();
    }

    /**
     * Get Total price of this cart.
     * @return
     */
    public float getTotalPrice() {
        float totalPrice = 0F;
        for (ItemOrder anOrder : orders)
            totalPrice += anOrder.getTotalPrice();
        return totalPrice;
    }

    public void addItem(Item anItem) {
        // if a menu is currently being built, add item to the menu.
        if (menuBeingBuild != null) {
            menuBeingBuild.getMenuItems().add(anItem);
            return;
        }
        // Otherwise add it as a normal item.
        for (ItemOrder anOrder : orders)
            if (anOrder.getTargetItem() == anItem) {
                anOrder.increaseQuantity();
                return;
            }
        orders.add(new ItemOrder(anItem, (byte)1));
    }

    /**
     * Indicates that next items to add will be part of a menu.
     * @param menu the menu to build.
     */
    public void setMenuBeingBuild(Menu menu) {
        menuBeingBuild = new MenuOrder(menu);
    }

    public boolean hasMenuBeingBuilt() {
        return menuBeingBuild != null;
    }

    public void validateMenuBeingBuilt() {
        if (menuBeingBuild == null)
            return;
        orders.add(menuBeingBuild);
        menuBeingBuild = null;
    }

    public void removeItemWithCategoryFromMenuBeingBuilt(ItemCategories itemCategories) {
        if (menuBeingBuild != null)
            menuBeingBuild.removeItemWithCategory(itemCategories);
    }

    public void clear() {
        menuBeingBuild = null;
        orders.clear();
    }

    public void removeItem(Item targetItem) {
        // browse items.
        for (ItemOrder anOrder : orders) {
            if (anOrder.getTargetItem() != targetItem)
                continue;

            // Decrement the quantity
            anOrder.decreaseQuantity();
            if (anOrder.getQuantity() > 0)
                return;

            // if there is no more items, remove it.
            orders.remove(anOrder);
            break;
        }
    }

    /**
     * Without modifying this cart, creates a list of sales to save to commit the transaction.
     * @param paymentMethod THe payment method client used.
     * @param sellingPoint the sellingPoint the sale was concluded.
     * @return the list of Sale items to save in database.
     */
    public List<Sale> commitSales(short paymentMethod, final SellingPoint sellingPoint) {

        LinkedList<Item> itemsForNormalSale = new LinkedList<Item>();
        LinkedList<Sale> salesToCommit = new LinkedList<Sale>();

        for (ItemOrder anOrder : orders) {
            // Items bought within a menu have to be included separately because the payment method
            // differ. We add then to notify stock movement.
            if (anOrder instanceof MenuOrder) {
                MenuOrder menuOrder = (MenuOrder) anOrder;
                Sale aMenuSale = new Sale((short) -1, new Date(), menuOrder.getTargetItem().getId(),
                        menuOrder.getMenuItems().toArray(new Item[0]), sellingPoint);
                salesToCommit.add(aMenuSale);

                // The menu itself is really sold. Add it to the normal sales.
                itemsForNormalSale.add(anOrder.getTargetItem());
                continue;
            }
            // Normal items are just added to the big list.
            for (int i = 0; i < anOrder.getQuantity(); ++i)
                itemsForNormalSale.add(anOrder.getTargetItem());
        }
        // Finally, create the normal sale to commit.
        Sale aSale = new Sale((short)-1, new Date(), paymentMethod,
                itemsForNormalSale.toArray(new Item[0]), sellingPoint);
        salesToCommit.add(aSale);
        return salesToCommit;
    }

    public void cancelMenuBeingBuilt() {
        menuBeingBuild = null;
    }
}
