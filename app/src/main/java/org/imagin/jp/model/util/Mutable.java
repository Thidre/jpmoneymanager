package org.imagin.jp.model.util;


import org.imagin.jp.model.map.SaleBoxer;
import org.imagin.jp.model.model.refer.Sale;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides mutable data.
 */
public class Mutable extends HttpInterlocutor {
    private static Mutable uniqueInstance;

    /**
     * Gets the unique instance of this Mutable.
     * @return
     */
    public static Mutable Get() {
        if (uniqueInstance == null)
            uniqueInstance = new Mutable();
        return uniqueInstance;
    }

    /**
     * Creates a new empty Mutable.
     */
    private Mutable() {
    }

    public boolean insertSale(Sale sale) {
        try {
            List<Sale> sales = new ArrayList<Sale>();
            sales.add(sale);
            return getProxy().insert(new SaleBoxer(sales));
        } catch (IOException e) {
            return false;
        }
    }

    public boolean insertSales(List<Sale> sales) {
        try {
            return getProxy().insert(new SaleBoxer(sales));
        } catch (IOException e) {
            return false;
        }
    }

}
