package org.imagin.jp.model.map;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Thibault on 01/03/2016.
 */
public abstract class ModelBoxerBase<T> implements IModelBoxer {

	private List<T> items;

	public final List<T> getItems() { return items; }

	protected List<T> getItems_() { return items; }

	/**
	 * Creates a new ModelBoxerBase.
	 * This boxer encapsulate provided list. It will use it for in/out operations.
	 *
	 * @param inOutList
	 */
	public ModelBoxerBase(List<T> inOutList) {
		items = inOutList;
	}

	/**
	 * Creates a new ModelBoxerBase.
	 * This boxer will use an internal list to store retrieved data.
	 */
	public ModelBoxerBase() {
		this(new ArrayList<T>());
	}

	/**
	 * Creates a new ModelBoxerBase.
	 * This boxer will use an internal list to store retrieved data.
	 *
	 * @param inItem item will be wrapped into the allocated list.
	 */
	public ModelBoxerBase(T inItem) {
		this(new ArrayList<T>());
		items.add(inItem);
	}

}
