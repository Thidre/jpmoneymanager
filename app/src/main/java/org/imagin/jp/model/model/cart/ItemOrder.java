package org.imagin.jp.model.model.cart;

import org.imagin.jp.model.model.refer.Item;

/**
 * Simple order targeting a certain amount of an atomic item.
 */
public class ItemOrder {

    private Item targetItem;
    private byte quantity;

    public final Item getTargetItem() {
        return targetItem;
    }

    public byte getQuantity() {
        return quantity;
    }

    public void increaseQuantity() {
        ++quantity;
    }
    /**
     * Creates a new ItemOrder for provided item and quantity.
     * @param targetItem item targeted by the order.
     * @param quantity quantity of item embedded.
     */
    public ItemOrder(Item targetItem, byte quantity) {
        this.targetItem = targetItem;
        this.quantity = quantity;
    }

    public float getTotalPrice() {
        return targetItem.getPrice() * quantity;
    }

    public void decreaseQuantity() {
        if (quantity > 0)
            --quantity;
    }
}
