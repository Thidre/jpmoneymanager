package org.imagin.jp.model.util;

import org.imagin.jp.model.map.ItemBoxer;
import org.imagin.jp.model.map.MenuBoxer;
import org.imagin.jp.model.map.SellingPointBoxer;
import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp.model.model.refer.Menu;
import org.imagin.jp.model.model.refer.SellingPoint;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Repository for nomenclature data.
 * Created by Thibault on 15/02/2016.
 */
public class Refer extends HttpInterlocutor {
    private static Refer uniqueInstance;

    /**
     * Gets the unique instance of this Refer.
     * @return
     */
    public static Refer Get() {
        if (uniqueInstance == null)
            uniqueInstance = new Refer();
        return uniqueInstance;
    }
    private boolean isInitialized;
    /**
     * Set of immutable items.
     */
    private Map<Short, Item> items;
    /**
     * Set of immutable menus.
     */
    private List<Menu> menus;
    /**
     * Set of immutable SellingPoint.
     */
    private List<SellingPoint> sellingPoints;

    public final Item getItem(short id) {
       return items.get(id);
    }
    public final Item getItemOrMenu(short id) {
        Item item = items.get(id);
        return item == null ? getMenu(id) : item;
    }


    public final Menu getMenu(short id) {
        for (Menu aMenu : menus)
            if (aMenu.getId() == id)
                return aMenu;
        return null;
    }
    public final SellingPoint getSellingPoint(short id) {
       for (SellingPoint sellingPoint : sellingPoints)
           if (sellingPoint.getId() == id)
               return sellingPoint;
       return null;
    }


    /**
     * Gets the whole set of items.
     * @return
     */
    public final Iterable<Item> getAllItems() {  return items.values(); }

    /**
     * Gets the whole set of menus.
     * @return
     */
    public final Iterable<Menu> getAllMenus() {
        return menus;
    }

    /**
     * Gets the whole set of menus.
     * @return
     */
    public final Iterable<SellingPoint> getAllSellingPoints() {
        return sellingPoints;
    }

    public boolean IsInitialized() { return isInitialized; }

    /**
     * Creates a new empty Refer.
     */
    private Refer() {
        items = new HashMap<Short, Item>(50);
        menus = new ArrayList<Menu>(10);
        sellingPoints = new ArrayList<SellingPoint>(10);
        isInitialized = false;
    }

    /**
     * Clear and refill Refer from Datasource.
     */
    public void Init() {
        // first clear.
        menus.clear();
        items.clear();
        sellingPoints.clear();
        isInitialized = false;

        // then do requests. If they success, list will be fulfilled with data.
        ItemBoxer itemBoxer = new ItemBoxer();
        MenuBoxer menuBoxer = new MenuBoxer(menus);
        SellingPointBoxer sellingPointBoxer = new SellingPointBoxer(sellingPoints);
        try {
            getProxy().getAll(itemBoxer);
            for (Item anItem : itemBoxer.getItems())
                items.put(anItem.getId(), anItem);
            getProxy().getAll(menuBoxer);
            getProxy().getAll(sellingPointBoxer);
            isInitialized = true;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
