package org.imagin.jp.model.map;

import android.util.Base64;

import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp.model.model.refer.ItemCategories;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Thibault on 29/02/2016.
 * <p/>
 * Provides Question support to interact with Datasource.
 */
public class ItemBoxer extends ModelBoxerBase<Item> {

	@Override
	public String getModelKey() { return "items"; }

	@Override
	public String getObjectsAsJson() {
		JSONArray jsonArray = new JSONArray();

		try {
			for (Item item : getItems_()) {
				JSONObject jsonObject = new JSONObject();
				jsonArray.put(jsonObject);
				jsonObject.put("id", item.getId());
				jsonObject.put("name", item.getName());
				jsonObject.put("category", ItemCategories.asInt(item.getCategory()));
				jsonObject.put("price", item.getPrice());
				// jsonObject.put("image", new String()); We won't do that anyway.
				return jsonArray.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public boolean fillWithJson(String jsonString) {
		getItems_().clear();
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); ++i) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				Item item = new Item(
						(short)jsonObject.getInt("id"),
						jsonObject.getString("name"),
						(float) jsonObject.getDouble("price"),
						ItemCategories.fromInteger(jsonObject.getInt("category")),
						Base64.decode(jsonObject.getString("image"), Base64.DEFAULT));
				getItems_().add(item);
			}
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Creates a new ItemBoxer.
	 * This boxer encapsulate provided list. It will use it for in/out operations.
	 *
	 * @param inOutList
	 */
	public ItemBoxer(List<Item> inOutList) {
		super(inOutList);
	}

	/**
	 * Creates a new ItemBoxer.
	 * This boxer will use an internal list to store retrieved data.
	 */
	public ItemBoxer() {
		super();
	}
}
