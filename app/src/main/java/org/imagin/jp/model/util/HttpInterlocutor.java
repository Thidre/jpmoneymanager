package org.imagin.jp.model.util;

import org.imagin.jp.model.map.HttpModelProxy;

/**
 * Created by Thibault on 01/03/2016.
 *
 * Stands for an element that dialogs using HTTP requests.
 */
public class HttpInterlocutor {

    /**
     * Proxy used to communicate.
     */
    private HttpModelProxy proxy;

    /**
     * Gets Proxy used to communicate through HTTP.
     * @return
     */
    protected HttpModelProxy getProxy() { return proxy; }

    /**
     * Creates a new HttpInterlocutor.
     */
    protected HttpInterlocutor() {
        proxy = new HttpModelProxy();
    }
}
