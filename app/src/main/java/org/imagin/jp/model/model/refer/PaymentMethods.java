package org.imagin.jp.model.model.refer;

import java.io.Serializable;

/**
 * Specify each of constant payment methods.
 *
 * @author Thibault
 */
public abstract class PaymentMethods {
	/**
	 * Unspecified.
	 */
	public final static short Unknown = 1000;
	/**
	 * By cash.
	 */
	public final static short Cash = 1001;
	/**
	 * By Cheque or as of.
	 */
	public final static short Cheque = 1002;
	/**
	 * Credit card.
	 */
	public final static short Card = 1003;
	/**
	 * Already paid.
	 */
	public final static short Prepaid = 1004;
	/**
	 * It's your lucky day !
	 */
	public final static short Free = 1005;
}
