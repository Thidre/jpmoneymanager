package org.imagin.jp.model.model.refer;

/**
 * Ensemble des produits pouvant etre vendus dans un repere spatio-temporel.
 *
 * @author Thibault
 */
public class Menu extends Item {
	/**
	 * Categories that are include in menu.
	 * Each category stands for a menu component that can be added to it.
	 * a category can appear several times.
	 */
	private ItemCategories[] categories;

	public ItemCategories[] getCategories() {
		return categories;
	}


	/**
	 * Creates a new Menu.
	 *
	 * @param name
	 * @param categories
	 * @param imageBytes
	 */
	public Menu(short id, String name, ItemCategories[] categories, float price, byte[] imageBytes) {
		super(id, name, price, ItemCategories.Menu, imageBytes);
		this.categories = categories;
	}
}
