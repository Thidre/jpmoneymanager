package org.imagin.jp.model.map;

/**
 * Created by Thibault on 29/02/2016.
 */
public enum ModelActions {
	GetAll, Insert;

	public static String toString(ModelActions action) {
		switch (action) {
			case GetAll: return "/all";
			case Insert: return "/insert";
			default: return "/";
		}
	}
}
