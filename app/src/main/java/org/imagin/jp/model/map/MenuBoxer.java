package org.imagin.jp.model.map;

import android.util.Base64;

import org.imagin.jp.model.model.refer.ItemCategories;
import org.imagin.jp.model.model.refer.Menu;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Thibault on 29/02/2016.
 * <p/>
 * Provides Question support to interact with Datasource.
 */
public class MenuBoxer extends ModelBoxerBase<Menu> {

	@Override
	public String getModelKey() { return "menus"; }

	@Override
	public String getObjectsAsJson() {
		JSONArray jsonArray = new JSONArray();

		try {
			for (Menu menu : getItems_()) {
				JSONArray categories = new JSONArray();
				for (ItemCategories category : menu.getCategories())
					categories.put(ItemCategories.asInt(category));
				JSONObject jsonObject = new JSONObject();
				jsonArray.put(jsonObject);
				jsonObject.put("id", menu.getId());
				jsonObject.put("name", menu.getName());
				jsonObject.put("categories", categories);
				jsonObject.put("price", menu.getPrice());
				// jsonObject.put("image", new String(menu.getImageBytes())); we wont do that anyway.
				return jsonArray.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public boolean fillWithJson(String jsonString) {
		getItems_().clear();
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); ++i) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				JSONArray jSONCategories = jsonObject.getJSONArray("categories");
				ItemCategories[] categories = new ItemCategories[jSONCategories.length()];
				for (int j = 0; j < jSONCategories.length(); ++j)
					categories[j] = ItemCategories.fromInteger(jSONCategories.getInt(j));
				Menu menu = new Menu(
						(short)jsonObject.getInt("id"),
						jsonObject.getString("name"),
						categories,
						(float) jsonObject.getDouble("price"),
						Base64.decode(jsonObject.getString("image"), Base64.DEFAULT));
				getItems_().add(menu);
			}
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Creates a new MenuBoxer.
	 * This boxer encapsulate provided list. It will use it for in/out operations.
	 *
	 * @param inOutList
	 */
	public MenuBoxer(List<Menu> inOutList) {
		super(inOutList);
	}

	/**
	 * Creates a new MenuBoxer.
	 * This boxer will use an internal list to store retrieved data.
	 */
	public MenuBoxer() {
		super();
	}
}
