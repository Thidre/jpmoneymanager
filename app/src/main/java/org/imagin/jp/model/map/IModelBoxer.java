package org.imagin.jp.model.map;

/**
 * Created by Thibault on 29/02/2016.
 */
public interface IModelBoxer {

	/**
	 * Obtains keyword for requiring objects of this nature.
	 *
	 * @return
	 */
	public String getModelKey();

	/**
	 * Return a Json-ified version of objects list stored in this IModelBoxer.
	 *
	 * @return
	 */
	public String getObjectsAsJson();

	/**
	 * Fill this IModelBoxer with a list of objects stored as a Json String.
	 *
	 * @param jsonString
	 * @return True if everything went fine.
	 */
	public boolean fillWithJson(String jsonString);
}
