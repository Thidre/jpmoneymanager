package org.imagin.jp.model.model.refer;


import org.imagin.jp.model.model.PortraitObject;

/**
 * Stands for a atomic component that can be sold.
 *
 * @author Thibault
 */
public class Item extends PortraitObject {

	/**
	 * Unit price of the item.
	 */
	private float price;
	/**
	 * Let item be gathered in coherent groups.
	 */
	private ItemCategories category;

	public float getPrice() { return price; }

	public ItemCategories getCategory() {
		return category;
	}

	/**
	 * Creates a new Item.
	 *
	 * @param name
	 * @param price
	 * @param category
	 * @param imageBytes
	 */
	public Item(short id, String name, float price, ItemCategories category, byte[] imageBytes) {
		super(id, name, imageBytes);
		this.price = price;
		this.category = category;
	}

	public Item(Item item) {
		super(item);
		this.price = item.price;
		this.category = item.category;
	}
}
