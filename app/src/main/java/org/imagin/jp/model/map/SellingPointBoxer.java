package org.imagin.jp.model.map;

import android.util.Base64;

import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp.model.model.refer.SellingPoint;
import org.imagin.jp.model.util.Refer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Thibault on 29/02/2016.
 * <p/>
 * Provides Question support to interact with Datasource.
 */
public class SellingPointBoxer extends ModelBoxerBase<SellingPoint> {

	@Override
	public String getModelKey() { return "sellingpoints"; }

	@Override
	public String getObjectsAsJson() {
		JSONArray jsonArray = new JSONArray();

		try {
			for (SellingPoint sellingPoint : getItems_()) {
				JSONArray items = new JSONArray();
				for (Item item : sellingPoint.getItems())
					items.put(item.getId());
				JSONObject jsonObject = new JSONObject();
				jsonArray.put(jsonObject);
				jsonObject.put("id", sellingPoint.getId());
				jsonObject.put("name", sellingPoint.getName());
				jsonObject.put("items", items);
				// jsonObject.put("image", new String(sellingPoint.getImageBytes())); We won't do that anyway.
				return jsonArray.toString();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public boolean fillWithJson(String jsonString) {
		getItems_().clear();
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); ++i) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				JSONArray jSONItems = jsonObject.getJSONArray("items");
				Item[] items = new Item[jSONItems.length()];
				for (int j = 0; j < jSONItems.length(); ++j)
					items[j] = Refer.Get().getItemOrMenu((short)jSONItems.getInt(j));
				SellingPoint sellingPoint = new SellingPoint(
						(short)jsonObject.getInt("id"),
						jsonObject.getString("name"),
						items);
				getItems_().add(sellingPoint);
			}
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Creates a new SellingPointBoxer.
	 * This boxer encapsulate provided list. It will use it for in/out operations.
	 *
	 * @param inOutList
	 */
	public SellingPointBoxer(List<SellingPoint> inOutList) {
		super(inOutList);
	}

	/**
	 * Creates a new SellingPointBoxer.
	 * This boxer will use an internal list to store retrieved data.
	 */
	public SellingPointBoxer() {
		super();
	}
}
