package org.imagin.jp.model.model.cart;

import org.imagin.jp.model.model.refer.ItemCategories;
import org.imagin.jp.model.model.refer.Menu;

/**
 * Defines how the PickItemActivity is supposed to play.
 */
public class PickItemScenario {

    private byte sceneIndex;
    private ItemCategories[] categoriesToPick;
    private boolean multiSelection;

    /**
     * Empty scenario.
     */
    public static final PickItemScenario Empty = new PickItemScenario(ItemCategories.None);

    /**
     * Get current category, the one for which user is expected to pick an item.
     * @return
     */
    public ItemCategories currentCategory() {
        return sceneIndex >= categoriesToPick.length ?
                ItemCategories.None : categoriesToPick[sceneIndex];
    }

    /**
     * Advance the cursor on the next category.
     * @return True if there is at least one more item to pick.
     */
    public boolean goForward() {
        return ++sceneIndex < categoriesToPick.length;
    }

    /**
     * Go one step backward in the scenario.
     */
    public void goBackward() {
        if (sceneIndex > 0)
            --sceneIndex;
    }

    /**
     * Indicates whether user is allowed to select several items.
     * @return true if user is allowed to pick several items.
     */
    public boolean isMultiSelection() {
        return multiSelection;
    }

    /**
     * Creates a new scenario for the provided menu.
     * scenario is single selection and includes menu categories.
     * @param menu the menu to create the scenario.
     */
    public PickItemScenario(final Menu menu) {
        sceneIndex = 0;
        this.categoriesToPick = menu.getCategories().clone();
    }

    /**
     * Creates a new scenario for the provided category only.
     * scenario is multi selection.
     * @param category the category to pick.
     */
    public PickItemScenario(ItemCategories category) {
        sceneIndex = 0;
        this.categoriesToPick = new ItemCategories[] { category };
        this.multiSelection = true;
    }
    /**
     * Creates a new scenario for the provided category only.
     * scenario is multi selection.
     * @param category the category to pick.
     * @param isMultiSelection Tells whether multi-selection is allowed.
     */
    public PickItemScenario(ItemCategories category, boolean isMultiSelection) {
        sceneIndex = 0;
        this.categoriesToPick = new ItemCategories[] { category };
        this.multiSelection = isMultiSelection;
    }

    /**
     * Indicates whether the scenario is only beginning or not.
     * @return true if the current step is the first one.
     */
    public boolean isFirstStep() {
        return sceneIndex == 0;
    }
}
