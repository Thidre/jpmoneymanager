package org.imagin.jp.model.model.refer;

import org.imagin.jp.model.model.IdName;

import java.util.Date;


public class Sale extends IdName {
	private Date date;
	private short paymentMethod;
	private Item[] items;
	private SellingPoint sellingPoint;

	/**
	 * Obtient la date de la vente.
	 *
	 * @return
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Definit la date de la vente.
	 *
	 * @param date
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Obtient la methode de paiement utilisee.
	 *
	 * @return
	 */
	public short getMethod() {
		return paymentMethod;
	}

	/**
	 * Definit la methode de paiement utilisee.
	 *
	 * @param paymentMethod
	 */
	public void setMethod(short paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	/**
	 * @return
	 */
	public final Item[] getItems() {
		return items;
	}

	public SellingPoint getSellingPoint() {
		return sellingPoint;
	}

	public void setSellingPoint(SellingPoint sellingPoint) {
		this.sellingPoint = sellingPoint;
	}

	/**
	 * @param items
	 */
	public void setItems(Item[] items) {
		this.items = items;
	}

	public Sale(short id, Date date, short paymentMethod, Item[] items, SellingPoint sellingPoint) {
		super(id, "");
		this.date = date;
		this.paymentMethod = paymentMethod;
		this.items = items;
		this.sellingPoint = sellingPoint;
	}
}
