package org.imagin.jp.model.model.cart;

import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp.model.model.refer.ItemCategories;
import org.imagin.jp.model.model.refer.Menu;

import java.util.LinkedList;
import java.util.List;

/**
 * Order that targets a menu. This menu has to be detailed with its own components.
 */
public class MenuOrder extends ItemOrder {

    /**
     * Components of the menu.
     */
    private List<Item> menuItems;

    /**
     *
     * @return Get user's selection of items to compose the menu.
     */
    public final List<Item> getMenuItems() {
        return menuItems;
    }

    /**
     * Creates a new MenuOrder for provided item and quantity.
     *
     * @param targetMenu menu targeted by the order.
     */
    public MenuOrder(Menu targetMenu) {
        super(targetMenu,(byte)1);
        menuItems = new LinkedList<Item>();
    }

    public void removeItemWithCategory(ItemCategories itemCategories) {
        Item itemToBeRemoved = null;
        for(Item anItem : menuItems) {
            if (anItem.getCategory() == itemCategories) {
                itemToBeRemoved = anItem;
                break;
            }
        }
        if (itemToBeRemoved != null)
            menuItems.remove(itemToBeRemoved);
    }
}
