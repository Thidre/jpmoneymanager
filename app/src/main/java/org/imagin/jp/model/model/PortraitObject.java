package org.imagin.jp.model.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Stands for an object that can be represented by an image and a name.
 */
public class PortraitObject extends IdName {

	/**
	 * PortraitObject raw image representation.
	 */
	private Bitmap bMap;

	public Bitmap getImage() { return bMap; }

	protected PortraitObject(short id, String name, byte[] imageBytes) {
		super(id, name);
		bMap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
	}

	protected PortraitObject(PortraitObject portrait) {
		super(portrait.getId(), portrait.getName());
		bMap = portrait.getImage();


	}


}
