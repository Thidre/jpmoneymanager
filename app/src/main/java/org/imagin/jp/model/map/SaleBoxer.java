package org.imagin.jp.model.map;

import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp.model.model.refer.Sale;
import org.imagin.jp.model.util.Refer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

/**
 * Created by Thibault on 29/02/2016.
 * <p/>
 * Provides Question support to interact with Datasource.
 */
public class SaleBoxer extends ModelBoxerBase<Sale> {

	@Override
	public String getModelKey() { return "sales"; }

	@Override
	public String getObjectsAsJson() {
		JSONArray jsonArray = new JSONArray();

		try {
			for (Sale sale : getItems_()) {
				JSONArray items = new JSONArray();
				for (Item item : sale.getItems())
					items.put(item.getId());
				JSONObject jsonObject = new JSONObject();
				jsonArray.put(jsonObject);
				jsonObject.put("id", sale.getId());
				jsonObject.put("date", sale.getDate().getTime() / 1000);
				jsonObject.put("payment_method", sale.getMethod());
				jsonObject.put("sellingpoint_id", sale.getSellingPoint().getId());
				jsonObject.put("items", items);
			}
			return jsonArray.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public boolean fillWithJson(String jsonString) {
		getItems_().clear();
		try {
			JSONArray jsonArray = new JSONArray(jsonString);
			for (int i = 0; i < jsonArray.length(); ++i) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				JSONArray jSONItems = jsonObject.getJSONArray("categories");
				Item[] items = new Item[jSONItems.length()];
				for (int j = 0; j < jSONItems.length(); ++j)
					items[j] = Refer.Get().getItem((short)jSONItems.getInt(j));
				Sale sale = new Sale(
						(short)jsonObject.getInt("id"),
						new Date(jsonObject.getInt("date") * 1000),
						(short)jsonObject.getInt("payment_method"),
						items,
						Refer.Get().getSellingPoint((short)jsonObject.getInt("sellingpoint_id")));
				getItems_().add(sale);
			}
			return true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Creates a new SaleBoxer.
	 * This boxer encapsulate provided list. It will use it for in/out operations.
	 *
	 * @param inOutList
	 */
	public SaleBoxer(List<Sale> inOutList) {
		super(inOutList);
	}

	/**
	 * Creates a new SaleBoxer.
	 * This boxer will use an internal list to store retrieved data.
	 */
	public SaleBoxer() {
		super();
	}
}
