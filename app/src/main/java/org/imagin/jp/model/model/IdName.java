package org.imagin.jp.model.model;

/**
 * Simple doubly identified object.
 *
 * @author Thibault
 */
public abstract class IdName {

    /**
     * Temporary Id attributed to objects yet created but not in the database.
     */
    public static final int NEW_OBJECT_ID = -1;

    /*
    Unique id of an object among its table in database.
     */
    private short id;
    /**
     * Helps identify an object with a String.
     */
    private String name;

    /**
     * Get this object Id.
     *
     * @return
     */
    public short getId() {
        return id;
    }

    /**
     * Defines this object Id.
     *
     * @param id
     */
    public void setId(short id) {
        this.id = id;
    }

    /**
     * Gets this object keyword. It may be empty but never null.
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Default constructor, for an IdName probably just created from scratch.
     */
    public IdName() {
        id = NEW_OBJECT_ID;
        name = "";
    }

    /**
     * Creates a new IdName with already set name and id.
     *
     * @param id
     * @param name
     */
    public IdName(short id, String name) {
        this.id = id;
        this.name = name;
    }
}
