package org.imagin.jp.model.model.refer;

import org.imagin.jp.model.model.IdName;

/**
 * Stands for a physical selling point. It tells which items can be sold at that place.
 *
 * @author Thibault
 */
public class SellingPoint extends IdName {

	/**
	 * Items sold at this selling point.
	 */
	private Item[] items;

	/**
	 * Get items sold at this selling point.
	 * @return
	 */
	public Item[] getItems() { return items; }

	/**
	 * Creates a new SellingPoint.
	 *
	 * @param id		 Unique id of this selling point.
	 * @param name       SellingPoint name
	 * @param items      Items that can be sold here.
	 */
	public SellingPoint(short id, String name, Item[] items) {
		super(id, name);
		this.items = items == null ? new Item[0] : items;
	}
}
