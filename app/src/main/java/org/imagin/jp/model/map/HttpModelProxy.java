package org.imagin.jp.model.map;

import android.os.StrictMode;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Thidre
 *         Make the link between local and remote business Model.
 */
public class HttpModelProxy {
	/*
	* Default hostname. Should be modifiable in app.
	*/
	//private static final String HOST = "http://jprefer.japan-party.net/";
	private static final String HOST = "http://192.168.1.29/jprefer/";

	private static String hostCurrent = HOST;
	/**
	 * Default unique client.
	 */
	private static HttpClient client;

	/**
	 * Get default Http client.
	 *
	 * @return default client.
	 */
	private static HttpClient getClient() {
		if (client == null) {
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used.
			int timeoutConnection = 1000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 2000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			client = new DefaultHttpClient(httpParameters);
		} return client;
	}

	public static String getHostCurrent() { return hostCurrent; }

	public static void setHostCurrent(String value) { hostCurrent = value; }

	private String buildUrl(IModelBoxer boxer, ModelActions action) { return getHostCurrent() + boxer.getModelKey() + ModelActions.toString(action); }

	private String send(HttpRequestBase httpRequest) throws IOException {
		try {
			HttpResponse response = getClient().execute(httpRequest);
			BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder sb = new StringBuilder();
			for (String newLine = in.readLine(); newLine != null; newLine = in.readLine()) {
				sb.append(newLine);
				sb.append("\r\n");
			}
			return sb.toString();
		} catch (Throwable err) {
			throw new IOException("Une exception est survenue durant l'exécution de la requête HTTP.");
		}
	}

	public boolean getAll(IModelBoxer boxer) throws IOException {
		return boxer.fillWithJson(send(new HttpGet(buildUrl(boxer, ModelActions.GetAll))));
	}

	public boolean insert(IModelBoxer boxer) throws IOException {
		HttpPost post = new HttpPost(buildUrl(boxer, ModelActions.Insert));
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("data", boxer.getObjectsAsJson()));
		post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		String acknowledgment = send(post);
		return acknowledgment.startsWith("OK");
	}
}

