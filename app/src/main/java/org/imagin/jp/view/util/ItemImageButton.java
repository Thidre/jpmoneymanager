package org.imagin.jp.view.util;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.imagin.jp.model.model.PortraitObject;
import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp5.R;


/**
 * Displays a thumb related to an item.
 * Can also store a quantity (quantity of that item).
 *
 * @author Thibault
 */
public class ItemImageButton extends FrameLayout {

	private TextView quantity_TV;

	/**
	 * Represented object.
	 */
	private final Item item;
	/**
	 * Defines a quantity for that item.
	 */
	private byte quantity;

	/**
	 * Increase quantity stored for the item.
	 */
	public void increaseQuantity() {
		++quantity;
		quantity_TV.setText("" + quantity);
	}

	public void setQuantity(byte quantity) {
		this.quantity = quantity;
		quantity_TV.setText(quantity == 0 ? "" : "" + quantity);
	}

	/**
	 * Get item stored in that button.
	 *
	 * @return
	 */
	public final Item getItem() {
		return item;
	}

	public ItemImageButton(Context context, final Item item) {
		super(context);
		this.item = item;

	 	// Creates graphical components.
		// icon
		ImageView thumb = new ImageView(getContext());
		if (item.getImage() == null)
			thumb.setImageResource(R.drawable.item_default);
		else
			thumb.setImageBitmap(item.getImage());

		// label.
		TextView labelView = new TextView(getContext());
		labelView.setText(item.getName());
		labelView.setTextColor(Color.WHITE);
		labelView.setBackgroundColor(0x77000000);

		// Thumb with quantity
		quantity_TV = new TextView(getContext());
		quantity_TV.setTextColor(Color.WHITE);
		quantity_TV.setTextSize(30);
		quantity_TV.setBackgroundColor(context.getResources().getColor(R.color.color_quantity_thumb));

		addView(thumb, new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		addView(quantity_TV, new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.RIGHT));
		addView(labelView, new FrameLayout.LayoutParams(thumb.getDrawable().getIntrinsicWidth(), LayoutParams.WRAP_CONTENT, Gravity.BOTTOM));

		setPadding(5, 5, 5, 5);
		setSelected(false);
	}

}
