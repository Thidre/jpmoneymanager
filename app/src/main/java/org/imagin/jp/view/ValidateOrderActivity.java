package org.imagin.jp.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.imagin.jp.model.model.cart.ItemOrder;
import org.imagin.jp.model.model.refer.PaymentMethods;
import org.imagin.jp.model.model.refer.Sale;
import org.imagin.jp.view.util.SaleItemButton;
import org.imagin.jp5.R;

import java.util.Arrays;
import java.util.List;

/**
 * Activity meant to validate a sell.
 */
public class ValidateOrderActivity extends ImaginActivity {

    /**
     * Background task used to commit a sale in the database.
     */
    private class SendSaleTask extends AsyncTask<Sale, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Sale... params) {
            if (params[0].getItems().length == 0)
                return true;
            if (getMutable().insertSales(Arrays.asList(params))) {
                return true;
            }
            return false;
        }
    }

    private RadioGroup Payment_RG;
    private LinearLayout Selection_LL;
    private TextView Total_LB;

    @Override
    protected int layoutId() {
        return R.layout.activity_validate_order;
    }

    @Override
    protected void initComponents() {

        // Bind previous, reset and next.
        bindExitActivityHandler(findViewById(R.id.Prev_LB));
        bindEventHandler(findViewById(R.id.CompleteSale_LB), onCompleteSale_LB_Click);
        Payment_RG = findControlById(R.id.PaymentMethod_RG);

        // set RB tags;
        findControlById(R.id.Prepayed_RB).setTag(PaymentMethods.Prepaid);
        findControlById(R.id.Cheque_RB).setTag(PaymentMethods.Cheque);
        findControlById(R.id.Cash_RB).setTag(PaymentMethods.Cash);
        findControlById(R.id.CreditCard_RB).setTag(PaymentMethods.Card);
        findControlById(R.id.Free_RB).setTag(PaymentMethods.Free);

        // bind other controls.
        Selection_LL 	= findControlById(R.id.Selection_LL);
        Total_LB 		= findControlById(R.id.Total_LB);
        loadTotal();
    }

    private void loadTotal() {
        Selection_LL.removeAllViews();
        for (ItemOrder anOrder : getApp().getCart().getOrders()) {
            SaleItemButton button = new SaleItemButton(this, anOrder);
            Selection_LL.addView(button);
        }
        updateTotalLabel();
    }

    private void updateTotalLabel() {
        Total_LB.setText("Total: " + getApp().getCart().getTotalPrice() + "€");
    }

    private View.OnClickListener onCompleteSale_LB_Click = new View.OnClickListener() {
        public void onClick(View v) {

            showYesNo(getResources().getString(R.string.complete_sale_message), getResources().getString(R.string.complete_sale_confirm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    short paymentMethod = (Short) findControlById(Payment_RG.getCheckedRadioButtonId()).getTag();
                    List<Sale> salesToCommit = getApp().getCart().commitSales(paymentMethod, getApp().getSellingPointCurrent());
                    SendSaleTask sendSaleTask = new SendSaleTask();
                    sendSaleTask.execute(salesToCommit.toArray(new Sale[0]));
                    try {
                        if (sendSaleTask.get()) {
                            getApp().getCart().clear();
                            setResult(RESULT_OK, new Intent().putExtra("SOLD_SUCCESS", true));
                            finish();
                        } else
                            showMessage("Erreur", "Echec de l'enregistrement. Veuillez réessayer. le panier n'a pas été vidé.");
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
}

