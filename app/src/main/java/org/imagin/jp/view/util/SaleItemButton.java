package org.imagin.jp.view.util;

import org.imagin.jp.model.model.cart.ItemOrder;
import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp5.R;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Indique la quantite d'un element dans une vente/commande et permet d'en retirer.
 * @author Thibault
 *
 */
public class SaleItemButton extends FrameLayout
{
	private static final int DEFAULT_IMAGE_ID = R.drawable.ptdflt;

	TextView labelView;
	ItemOrder itemOrder;

	public Item getTargetItem() {
		return itemOrder.getTargetItem();
	}

	public SaleItemButton(Context context, ItemOrder itemOrder) {
		super(context);
		this.itemOrder = itemOrder;
		init();
	}

	public void updateText() {
		labelView.setText(getTargetItem().getName() + "\n" + itemOrder.getQuantity() + "x" + getTargetItem().getPrice() + "€");
	}
	
	private void init() {

		// icon
		ImageView thumb = new ImageView(getContext());
		if (getTargetItem().getImage() == null)
			thumb.setImageResource(R.drawable.item_default);
		else
			thumb.setImageBitmap(getTargetItem().getImage());

		// label.
		labelView = new TextView(getContext());
		labelView.setTextColor(Color.WHITE);
		labelView.setBackgroundColor(0x77000000);
		updateText();

		addView(thumb, new FrameLayout.LayoutParams(200, 200));
		addView(labelView, new FrameLayout.LayoutParams(200, LayoutParams.WRAP_CONTENT, Gravity.BOTTOM));
		setPadding(5, 5, 5, 5);
		
	}

	public ItemOrder getItemOrder() {
		return itemOrder;
	}
}
