package org.imagin.jp.view;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.apmem.tools.layouts.FlowLayout;
import org.imagin.jp.model.model.cart.PickItemScenario;
import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp.model.model.refer.ItemCategories;
import org.imagin.jp.model.model.refer.Menu;
import org.imagin.jp.view.util.ItemImageButton;
import org.imagin.jp5.R;

import java.util.LinkedList;

/**
 * Let user select items to put in its current order.
 * Only a single category of item is visible. This category
 * is determined by currentCategory member of App global object.
 *
 */
public class PickItemsActivity extends ImaginActivity {

	private LinkedList<Item> itemsSelected;
	private ItemImageButton lastItemImageSelected;

	@Override
	protected int layoutId() { return R.layout.activity_add_order_items; }

	@Override
	protected void initComponents() {

		itemsSelected = new LinkedList<Item>();

		// Bind previous, reset and next.
		bindEventHandler(findViewById(R.id.Previous_BT), onPrevious_BT_Click);
		bindEventHandler(findViewById(R.id.Reset_BT), onResetClick);
		bindEventHandler(findViewById(R.id.Next_TV), onNext_LB_Click);

		loadScenario();
	}
		
	private void loadScenario() {
		itemsSelected.clear();

		// Check category to pick.
		ItemCategories currentCategory = getApp().getPickItemScenario().currentCategory();
		if (currentCategory == ItemCategories.None) {
			finish();
			return;
		}
		((TextView)findViewById(R.id.Category_TV)).setText(ItemCategories.toString(currentCategory));

		// Add all items from current selling point with a matching category to the
		// one selected by user.
		ScrollView items_SV =  (ScrollView)findViewById(R.id.Items_SV);
		items_SV.removeAllViews();
		FlowLayout caisses_FL = new FlowLayout(getBaseContext());
		items_SV.addView(caisses_FL);
		boolean noItemsAtAll = true;
		for(Item anItem : getApp().getSellingPointCurrent().getItems()) {
			if (anItem.getCategory() != currentCategory)
				continue;

			noItemsAtAll = false;
			ItemImageButton bt = new ItemImageButton(getBaseContext(), anItem);
			bindEventHandler(bt, onProductClick);
			caisses_FL.addView(bt);
		};

		// if no items, display no results.
		if(noItemsAtAll)
			caisses_FL.addView(getNoResultsTV());
	}


	private View.OnClickListener onResetClick = new View.OnClickListener() {
		public void onClick(View v) {
			loadScenario();
		}
	};

	/**
	 * Se produit lorsqu'un produit est clique.
	 */
	private View.OnClickListener onProductClick = new View.OnClickListener() {
		public void onClick(View v) {

			// Check that multi selection is enabled or this is the first choice.
			if (!getApp().getPickItemScenario().isMultiSelection() && !itemsSelected.isEmpty()) {
				lastItemImageSelected.setQuantity((byte)0);
				itemsSelected.clear();
			}

			// Add item to the selected ones.
			lastItemImageSelected =(ItemImageButton)v;
			itemsSelected.add(lastItemImageSelected.getItem());
			lastItemImageSelected.increaseQuantity();

			// if user is only allowed to select one item, then finish activity.
			if (!getApp().getPickItemScenario().isMultiSelection()) {
				onNext_LB_Click.onClick(null);
			}
		}
	};

	private View.OnClickListener onPrevious_BT_Click = new View.OnClickListener() {
		public void onClick(View v) {
			// if this is the first step, just finish the activity
			if (getApp().getPickItemScenario().isFirstStep()) {
				getApp().getCart().cancelMenuBeingBuilt();
				setResult(RESULT_CANCELED);
				finish();
				return;
			}
			// Otherwise move backward and remove from the menu any selected item with current category.
			if (getApp().getCart().hasMenuBeingBuilt())
				getApp().getCart().removeItemWithCategoryFromMenuBeingBuilt(getApp().getPickItemScenario().currentCategory());
			getApp().getPickItemScenario().goBackward();
			loadScenario();
		}
	};

	private View.OnClickListener onNext_LB_Click = new View.OnClickListener() {
		public void onClick(View v) {

			// if current selected item is a menu, enter the menu mode.
			if (itemsSelected.size() == 1 && itemsSelected.getFirst() instanceof Menu) {
				Menu menu = (Menu) itemsSelected.getFirst();
				getApp().getCart().setMenuBeingBuild(menu);
				getApp().setPickItemScenario(new PickItemScenario(menu));
				loadScenario();
				return;
			}

			// Otherwise add selected items to the cart.
			for (Item anItem : itemsSelected)
				getApp().getCart().addItem(anItem);

			// Move scenario cursor.
			boolean isThereMore = getApp().getPickItemScenario().goForward();
			if (!isThereMore) {
				// if we completed a menu, validate it.
				if (getApp().getCart().hasMenuBeingBuilt())
					getApp().getCart().validateMenuBeingBuilt();
				setResult(RESULT_OK);
				finish();
				return;
			}

			//otherwise start again...
			loadScenario();
	} };

}
