package org.imagin.jp.view.util;

import android.content.Context;
import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import org.apmem.tools.layouts.FlowLayout;
import org.imagin.jp.model.model.refer.SellingPoint;
import org.imagin.jp5.R;

/**
 * Stands for a button displayed in Selling point menu.
 */
public class SellingPointButton extends Button {

    private SellingPoint sellingPoint;

    public SellingPoint getSellingPoint() {
        return sellingPoint;
    }

    public SellingPointButton(Context context, SellingPoint sellingPoint) {
        super(context);
        this.sellingPoint = sellingPoint;

        // Let's display a purple ribbon with white text.
        setText(sellingPoint.getName());
        setTextSize(30);
    }

}
