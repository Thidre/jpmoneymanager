package org.imagin.jp.view;

import org.apmem.tools.layouts.FlowLayout;
import org.imagin.jp.model.model.refer.SellingPoint;
import org.imagin.jp.view.util.SellingPointButton;
import org.imagin.jp5.R;

import android.content.Intent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ScrollView;

/**
 * Entry point for application.
 * @author Thibault
 *
 */
public class SelectCaisseActivity extends ImaginActivity {

	@Override
	protected int layoutId() { return org.imagin.jp5.R.layout.activity_select_caisse; }

	@Override
	protected void initComponents() throws Throwable
	{
		// Bind Options Button to Options activity.
		bindEventHandler(findControlById(R.id.Settings_BT), OnOptions_BT_Click);
		try {
			loadSellingPoints();
		} catch(Throwable err) {}
	}

	private void loadSellingPoints() throws Throwable {
		ScrollView caisses_SV = this.<ScrollView>findControlById(R.id.Caisses_SV);
		caisses_SV.removeAllViews();
		FlowLayout caisses_FL = new FlowLayout(getBaseContext());
		caisses_SV.addView(caisses_FL);

		// Retrieve SellingPoints
		Iterable<SellingPoint> sellingPoints = getRefer().getAllSellingPoints();
		if (!sellingPoints.iterator().hasNext())
			caisses_FL.addView(getNoResultsTV());

		for(SellingPoint aSellingPoint : getRefer().getAllSellingPoints()) {
			SellingPointButton bt = new SellingPointButton(this, aSellingPoint);
			bindEventHandler(bt, onMenuClick);
			caisses_FL.addView(bt, new FrameLayout.LayoutParams(200, 200));
		}
	}

	/**
	 * Happens during a click on a SellingPoint icon.
	 */
	private View.OnClickListener onMenuClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			SellingPointButton button = (SellingPointButton)v;
			getApp().setSellingPointCurrent(button.getSellingPoint());
			startNewActivity(ComposeOrderActivity.class);
		}
	};

	/**
	 * Happens on a click on Settings_BT. Start Options activity.
	 */
	protected View.OnClickListener OnOptions_BT_Click = new View.OnClickListener() {
		@Override
		public void onClick(View v){
			startNewActivityForResult(OptionsActivity.class);
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode) {
			case RESULT_OK:
				try {
					loadSellingPoints();
				} catch (Throwable err) {}
				break;
			case RESULT_CANCELED: break;
		}
	}
}