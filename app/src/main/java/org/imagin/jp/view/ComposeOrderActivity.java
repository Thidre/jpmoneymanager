package org.imagin.jp.view;

import org.imagin.jp.model.model.cart.ItemOrder;
import org.imagin.jp.model.model.cart.MenuOrder;
import org.imagin.jp.model.model.cart.PickItemScenario;
import org.imagin.jp.model.model.refer.Item;
import org.imagin.jp.model.model.refer.ItemCategories;
import org.imagin.jp.model.model.refer.Menu;
import org.imagin.jp.view.util.SaleItemButton;
import org.imagin.jp5.R;

import android.content.ClipData;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.HashSet;

/**
 * Let user compose its order.
 * See current order state and leads to item selection activity.
 * @author Thibault
 *
 */
public class ComposeOrderActivity extends ImaginActivity {

	/**
	 * Menu contenant les produits manipules sur cette activite.
	 */
	private LinearLayout Selection_LL;
	private TextView Total_LB;

	@Override
	protected int layoutId() { return R.layout.activity_compose_order; }

	@Override
	protected void initComponents() {
		
		// Bind previous, reset and next.
		bindExitActivityHandler(findViewById(R.id.Previous_BT));
		bindEventHandler(findViewById(R.id.Next_LB), onNextClick);
		bindEventHandler(findViewById(R.id.Reset_BT), onResetClick);

		// Bind buttons to compose the cart
		bindEventHandler(findViewById(R.id.AddUndefined_BT), onUndefined_BT_Click);
		bindEventHandler(findViewById(R.id.AddMain_BT), onMain_BT_Click);
		bindEventHandler(findViewById(R.id.AddGarniture_BT), onGarniture_BT_Click);
		bindEventHandler(findViewById(R.id.AddDrink_BT), onDrink_BT_Click);
		bindEventHandler(findViewById(R.id.AddDessert_BT), onDessert_BT_Click);
		bindEventHandler(findViewById(R.id.AddMenu_BT), onMenu_BT_Click);

		// check which items exist in selling point, and hide useless buttons.
		boolean[] itemCategoriesExist = new boolean[ItemCategories.values().length];
		for (Item anItem : getApp().getSellingPointCurrent().getItems()) {
			itemCategoriesExist[ItemCategories.asInt(anItem.getCategory())] = true;
		}
		for (int i = 0; i < ItemCategories.values().length; ++i)
			if (!itemCategoriesExist[i])
				switch (ItemCategories.fromInteger(i)) {
					case Unsorted:
						removeView(findViewById(R.id.AddUndefined_BT));
						break;
					case Main:
						removeView(findViewById(R.id.AddMain_BT));
						break;
					case Garniture:
						removeView(findViewById(R.id.AddGarniture_BT));
						break;
					case Drink:
						removeView(findViewById(R.id.AddDrink_BT));
						break;
					case Dessert:
						removeView(findViewById(R.id.AddDessert_BT));
						break;
					case Menu:
						removeView(findViewById(R.id.AddMenu_BT));
						break;
				}

		// bind other controls.
		Selection_LL 	= findControlById(R.id.Selection_LL);
		Total_LB 		= findControlById(R.id.Total_LB);
		loadTotal();
	}
		
	private void loadTotal() {
		Selection_LL.removeAllViews();
		for (ItemOrder anOrder : getApp().getCart().getOrders()) {
			SaleItemButton button = new SaleItemButton(this, anOrder);
			bindEventHandler(button, onOrderItemClick);
			Selection_LL.addView(button);
		}
		updateTotalLabel();
	}
	
	private void updateTotalLabel() {
		Total_LB.setText("Total: " + getApp().getCart().getTotalPrice() + "€");
	}

	private View.OnClickListener onResetClick = new View.OnClickListener() {
		public void onClick(View v) {
			getApp().getCart().clear();
			loadTotal();
		}
	};
	private View.OnClickListener onNextClick = new View.OnClickListener() {
		public void onClick(View v) {
			startNewActivityForResult(ValidateOrderActivity.class);
		}
	};
	private View.OnClickListener onUndefined_BT_Click = new View.OnClickListener() {
		public void onClick(View v) {
			// No multi selection for menus.
			getApp().setPickItemScenario(new PickItemScenario(ItemCategories.Unsorted));
			startNewActivityForResult(PickItemsActivity.class);
		}
	};
	private View.OnClickListener onMenu_BT_Click = new View.OnClickListener() {
		public void onClick(View v) {
			// No multi selection for menus.
			getApp().setPickItemScenario(new PickItemScenario(ItemCategories.Menu, false));
			startNewActivityForResult(PickItemsActivity.class);
		}
	};
	private View.OnClickListener onMain_BT_Click = new View.OnClickListener() {
		public void onClick(View v) {
			getApp().setPickItemScenario(new PickItemScenario(ItemCategories.Main));
			startNewActivityForResult(PickItemsActivity.class);
		}
	};
	private View.OnClickListener onDrink_BT_Click = new View.OnClickListener() {
		public void onClick(View v) {
			getApp().setPickItemScenario(new PickItemScenario(ItemCategories.Drink));
			startNewActivityForResult(PickItemsActivity.class);
		}
	};
	private View.OnClickListener onDessert_BT_Click = new View.OnClickListener() {
		public void onClick(View v) {
			getApp().setPickItemScenario(new PickItemScenario(ItemCategories.Dessert));
			startNewActivityForResult(PickItemsActivity.class);
		}
	};
	private View.OnClickListener onGarniture_BT_Click = new View.OnClickListener() {
		public void onClick(View v) {
			getApp().setPickItemScenario(new PickItemScenario(ItemCategories.Garniture));
			startNewActivityForResult(PickItemsActivity.class);
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode) {
			case RESULT_OK:
				loadTotal();
				if (data != null && data.hasExtra("SOLD_SUCCESS"))
					showMessage("Succès", "La vente est enregistrée.");
				break;
			case RESULT_CANCELED: break;
		}
	}

	/**
	 * Happens when a command item is clicked.
	 * Decrease the quantity of the selected item.
	 */
	private View.OnClickListener onOrderItemClick = new View.OnClickListener() {
		public void onClick(View v) {
			SaleItemButton sender = (SaleItemButton) v;
			getApp().getCart().removeItem(sender.getTargetItem());
			sender.updateText();
			updateTotalLabel();

			// remove element if no more useful.
			if (sender.getItemOrder().getQuantity() == 0)
				Selection_LL.removeView(sender);
		}
	};
}
