package org.imagin.jp.view;


import org.imagin.jp.model.util.Mutable;
import org.imagin.jp.model.util.Refer;
import org.imagin.jp.shared.App;
import org.imagin.jp5.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * Base class for all ImaginCon activities.
 * @author Thibault
 *
 */
public abstract class ImaginActivity extends Activity
{

	private class ReferLoadingTask extends  AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				Refer.Get().Init();
			} catch (Throwable err) {
				err.printStackTrace();
			}
			return true;
		}
	};

	protected Mutable getMutable() { return Mutable.Get(); }

	protected Refer getRefer() throws Throwable {
		if (!Refer.Get().IsInitialized()) {
			AlertDialog referLoadingAlert = new AlertDialog.Builder(self).setMessage("Chargement du Refer...").setTitle("Veuillez Patienter").create();
			referLoadingAlert.show();
			ReferLoadingTask referLoadingTask = new ReferLoadingTask();
			referLoadingTask.execute();
			referLoadingTask.get();
			referLoadingAlert.hide();
		}
		if (!Refer.Get().IsInitialized())
			throw new Throwable("Unable to load refer. Check Wifi connection.");
		return Refer.Get();
	}

	protected App getApp() { return App.Get(); }
	/**
	 * Provides id for this activity layout.
	 * @return
	 */
	protected abstract int layoutId();
	/**
	 * Use this method to initialize your controls.
	 */
	protected abstract void initComponents() throws Throwable;
	
	private Activity self = this;
	
	protected void showError(Throwable err) {
		new AlertDialog.Builder(self).setMessage(err.getMessage()).setTitle("Oups ! Une erreur s'est produite...").setPositiveButton("OK", null).create().show();
	}
	
	protected void showYesNo(String title, String message, DialogInterface.OnClickListener onPositiveClickListener) {
		new AlertDialog.Builder(self).setMessage(message).setTitle(title).setPositiveButton(R.string.complete, onPositiveClickListener).setNegativeButton(R.string.cancel, null).create().show();
	}
	protected void showMessage(String title, String message) {
		new AlertDialog.Builder(self).setMessage(message).setTitle(title).setPositiveButton("OK", null).create().show();
	}
	
	/**
	 * Safe version of findControlById.
	 * @param id Id of control you want to access.
	 * @return Control retrieved, or null if control was not found in current context.
	 */
	@SuppressWarnings("unchecked")
	protected <T extends View> T findControlById(int id) {
		return (T) findViewById(id);
	}
	
	/**
	 * Safe Event listener add. Wrap your listener in a try catch block.
	 * Use it for user action listener, as top event handler, to be sure to catch each exception.
	 * @param control
	 * @param listener
	 */
	protected void bindEventHandler(final View control, final OnClickListener listener)
	{
		control.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					listener.onClick(control);
				}
				catch (Throwable e) {
					showError(e);
				}
			}
		});
	}

	/**
	 * Shortcut for making a control a link exit current activity.
	 * @param control Control to bind.
	 */
	protected void bindExitActivityHandler(final View control)
	{
		bindEventHandler(control, new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(layoutId());
        try {

	        initComponents();
        } catch(Throwable err) {
        	showError(err);
        }
    }
	
	/**
	 * Shortcut for creating and launching an activity.
	 * @param activityClass class of activity to launch.
	 */
	protected void startNewActivity(Class<? extends Activity> activityClass)
	{
		// before transferring control, put current extras (user selection).
		Intent intent = new Intent(this, activityClass);
		intent.putExtras(getIntent());
		startActivity(intent);
	}

	/**
	 * Shortcut for creating and launching an activity.
	 * @param activityClass class of activity to launch.
	 */
	protected void startNewActivityForResult(Class<? extends Activity> activityClass)
	{
		// before transferring control, put current extras (user selection).
		Intent intent = new Intent(this, activityClass);
		intent.putExtras(getIntent());
		startActivityForResult(intent, 0);
	}
	
	/**
	 * Gets a TextView saying "no results" with a nickel style.
	 * @return
	 */
	protected TextView getNoResultsTV() {
		TextView tView = new TextView(this);
		tView.setText("There is nothing here yet... :-(");
		tView.setTextSize(20);
		tView.setTextColor(0xFF999999);
		tView.setPadding(10, 10, 0, 0);
		return tView;
	}

	protected void removeView(View v) {
		((ViewGroup)v.getParent()).removeView(v);
	}
}
