package org.imagin.jp.view;

import android.view.View;
import android.widget.EditText;

import org.imagin.jp.model.map.HttpModelProxy;
import org.imagin.jp.model.util.Refer;
import org.imagin.jp5.R;

/**
 * App entry point.
 * @author Thibault
 *
 */
public class OptionsActivity extends ImaginActivity {

	/**
	 * Edit text that contains Refer Url input by user.
	 */
	private EditText baseUrl_ET;

	@Override
	protected int layoutId() { return R.layout.activity_options; }

	@Override
	protected void initComponents() throws Throwable {
		// Prev button goes back to Selling point selection.
		bindExitActivityHandler(findControlById(R.id.Previous_BT));

		// Load TextView that contains URL, and bind update button supposed to update refer.
		baseUrl_ET = findControlById(R.id.ReferURL_ET);
		baseUrl_ET.setText(HttpModelProxy.getHostCurrent());
		bindEventHandler(findControlById(R.id.UpdateRefer_BT), OnUpdateRefer_BT_Click);
	}

	/**
	 * Happens when user clicks on UpdateRefer_BT.
	 * Updates host and refresh refer now.
	 */
	protected View.OnClickListener OnUpdateRefer_BT_Click = new View.OnClickListener() {
		@Override
		public void onClick(View v){
			HttpModelProxy.setHostCurrent(baseUrl_ET.getText().toString());
			Refer.Get().Init();
			if (Refer.Get().IsInitialized())
				showMessage("Succès", "Refer mis à jour.");
			else
				showMessage("Echec", "Impossible de mettre à jour le Refer.");
			setResult(RESULT_OK);
		}
	};
}