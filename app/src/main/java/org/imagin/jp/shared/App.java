package org.imagin.jp.shared;

import org.imagin.jp.model.model.cart.Cart;
import org.imagin.jp.model.model.cart.PickItemScenario;
import org.imagin.jp.model.model.refer.SellingPoint;

/**
 * Static singleton that holds user's current options and settings.
 */
public class App {
	/**
	 * Unique instance of this object.
	 */
	private static App uniqueInstance;

	/**
	 * Gets the unique instance of this Refer.
	 * @return
	 */
	public static App Get() {
		if (uniqueInstance == null) {
			uniqueInstance = new App();
		}
		return uniqueInstance;
	}

	private App() {
		this.cart = new Cart();
	}

	private SellingPoint sellingPointCurrent;
	private Cart cart;
	private PickItemScenario scenario;

	public SellingPoint getSellingPointCurrent() {
		return sellingPointCurrent;
	}

	/**
	 * Get cart being built.
	 * @return
	 */
	public Cart getCart() {
		return cart;
	}

	/**
	 * Get current scenario, or the empty one if none.
	 * @return the current pick scenario.
	 */
	public PickItemScenario getPickItemScenario() {
		return scenario == null ? PickItemScenario.Empty : scenario;
	}

	/**
	 * Defines current category for picking item in cart.
	 * @param pickItemScenario the new scenario	.
	 */
	public void setPickItemScenario(PickItemScenario pickItemScenario) { scenario = pickItemScenario; }

	/**
	 * Defines the current selling point.
	 * @param sellingPointCurrent the new selling point.
	 */
	public void setSellingPointCurrent(SellingPoint sellingPointCurrent) {
		this.sellingPointCurrent = sellingPointCurrent;
	}




}
